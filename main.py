"""This module use scikit learn to learn about white wine."""

import os
import subprocess
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from sklearn.metrics import accuracy_score


def k_neighbors(data):
    """Create K Neighbors

    Parameters
    ----------
    data: panda imported data.
    """

    print("-----------------------------------------------")
    print("K Neighbors")
    print("-----------------------------------------------")

    # Separate target from training features
    y_data = data.quality
    x_data = data.drop('quality', axis=1)

    # Split data into training and testing sets.
    x_train, x_test, y_train, y_test = train_test_split(
        x_data, y_data, test_size=0.2, random_state=123, stratify=y_data)

    # Testing diffenrt K
    for k_temp in range(25):
        k_value = k_temp + 1

        # Instantiate the model
        knn = KNeighborsClassifier(
            n_neighbors=k_value, weights='uniform', algorithm='auto')
        # Fit the model on the training data.
        knn.fit(x_train, y_train)
        y_pred = knn.predict(x_test)
        print("Accuracy is ", accuracy_score(y_test, y_pred)
              * 100, "% for K-Value:", k_value)

    print("***********************************************")


def dec_tree_class(data):
    """Create Decision Tree

    Parameters
    ----------
    data: panda imported data.
    """

    print("-----------------------------------------------")
    print("Decision Tree")
    print("-----------------------------------------------")

    # Separate target from training features
    y_data = data.quality
    x_data = data.drop('quality', axis=1)

    # Split data into training and testing sets.
    x_train, x_test, y_train, y_test = train_test_split(
        x_data, y_data, test_size=0.2, random_state=123, stratify=y_data)

    # Prevent pylint warning
    str(x_test)
    str(y_test)

    # Create Decision Tree
    tree_data = DecisionTreeClassifier(min_samples_split=20, random_state=100)
    tree_data.fit(x_train, y_train)

    # Create list of all colums
    feature_names = list(data)
    # Delete in train data dropped colum
    feature_names.remove('quality')
    # Vizualize tree
    visualize_tree(tree_data, feature_names)
    print("***********************************************")


def svm_classification(data):
    """Export all data as digram

    Parameters
    ----------
    data: panda imported data.
    """

    print("-----------------------------------------------")
    print("Support Vector Machines")
    print("-----------------------------------------------")

    # Since this is a classification problem between "Great" and "Not Great" wine,
    # we are going to separate our quality variable.
    # Above 6 will be 1
    # Below 6 will be 0
    data.ix[data.quality < 6, 'quality'] = 0
    data.ix[data.quality >= 6, 'quality'] = 1
    print("Bad Wine:", len(data[data.quality == 0]))
    print("Great Wine:", len(data[data.quality == 1]))
    # also do a test-train split so we can later check the accuracy of our model
    x_train, x_test, y_train, y_test = train_test_split(
        data[['pH', 'alcohol']], data['quality'], test_size=0.25, random_state=1)

    # Create SVM classification object
    model = svm.SVC(C=1, kernel='linear')

    model.fit(x_train, y_train)
    model.score(x_test, y_test)
    # Predict Output
    predicted = model.predict(x_test)
    print("Accuracy is", str(accuracy_score(y_test, predicted) * 100) + "%")

    directory = "output/svm/"

    if not os.path.exists(directory):
        os.makedirs(directory)

    # plot
    fig = plt.figure()
    axes_object = fig.add_subplot(111)
    x0_data, x1_data = x_train['pH'], x_train['alcohol']
    xx_data, yy_data = make_meshgrid(x0_data, x1_data)
    plot_contours(axes_object, model, xx_data, yy_data,
                  cmap=plt.get_cmap('coolwarm'), alpha=0.8)
    axes_object.scatter(x0_data, x1_data, c=y_train,
                        cmap=plt.get_cmap('coolwarm'), alpha=0.3)
    axes_object.set_xlabel('pH')
    axes_object.set_ylabel('Alcohol %')
    axes_object.set_title('SVM')
    plt.savefig(directory + '01-svm.png')
    plt.clf()

    model2 = SGDClassifier(tol=.001, fit_intercept=False)
    model2.fit(data[['pH', 'alcohol']], data['quality'])
    # plot
    fig = plt.figure()
    axes_object = fig.add_subplot(111)
    x0_data, x1_data = x_train['pH'], x_train['alcohol']
    xx_data, yy_data = make_meshgrid(x0_data, x1_data)
    plot_contours(axes_object, model2, xx_data, yy_data,
                  cmap=plt.get_cmap('coolwarm'), alpha=0.8)
    axes_object.scatter(x0_data, x1_data, c=y_train,
                        cmap=plt.get_cmap('coolwarm'), alpha=0.3)
    axes_object.set_xlabel('pH')
    axes_object.set_ylabel('Alcohol %')
    axes_object.set_title('SVM with weights')
    plt.savefig(directory + '02-svm_weights.png')
    plt.clf()

    print("***********************************************")


def output(data):
    """Print all Data

    Parameters
    ----------
    data: panda imported data.
    """

    print("-----------------------------------------------")
    print("Print all Data")
    print("-----------------------------------------------")

    print(data.head())
    print(data.shape)
    print(data.describe)

    # Generate digramm from all data
    output_all(data)

    print("***********************************************")


def output_all(data):
    """Export all data as digram

    Parameters
    ----------
    data: panda imported data.
    """
    directory = 'output/data/'
    counter = 0
    for row in data:
        counter = counter + 1
        count = str(counter)
        if counter < 10:
            count = count.zfill(2)

        plt.plot(data[row])
        plt.ylabel(row)
        plt.xlabel("Count")
        plt.title(row)
        if not os.path.exists(directory):
            os.makedirs(directory)
        plt.savefig(directory + count + '-' + row.replace(" ", "_") + '.png')
        plt.clf()


def visualize_tree(tree, feature_names):
    """Create tree png using graphviz.

    Parameters
    ----------
    tree: scikit-learn DecsisionTree.
    feature_names: list of feature names.
    """

    directory = "output/tree/"

    if not os.path.exists(directory):
        os.makedirs(directory)

    dotfile = open(directory + "dt.dot", 'w')
    export_graphviz(tree, out_file=dotfile, feature_names=feature_names)
    dotfile.close()
    print("Tree path:")
    print((os.path.realpath(dotfile.name).replace(".dot", ".png")))

    try:
        subprocess.check_call(
            "dot -T png output/tree/dt.dot -o output/tree/01-decsision_tree.png")
    except subprocess.CalledProcessError as error:
        print("Could not run dot, ie graphviz, to "
              "produce visualization.", error.output)


def make_meshgrid(x_data, y_data, stepsize=.02):
    """Create a mesh of points to plot in

    Parameters
    ----------
    x_data: data to base x-axis meshgrid on
    y_data: data to base y-axis meshgrid on
    stepsize: stepsize for meshgrid, optional

    Returns
    -------
    xx_data, yy_data : ndarray
    """

    x_min, x_max = x_data.min() - 1, x_data.max() + 1
    y_min, y_max = y_data.min() - 1, y_data.max() + 1
    xx_data, yy_data = np.meshgrid(np.arange(x_min, x_max, stepsize),
                                   np.arange(y_min, y_max, stepsize))
    return xx_data, yy_data


def plot_contours(axes_object, clf, xx_meshgrid, yy_meshgrid, **params):
    """Plot the decision boundaries for a classifier.

    Parameters
    ----------
    axes_object: matplotlib axes object
    clf: a classifier
    xx_meshgrid: meshgrid ndarray
    yy_meshgrid: meshgrid ndarray
    params: dictionary of params to pass to contourf, optional
    """

    decision_boundaries = clf.predict(
        np.c_[xx_meshgrid.ravel(), yy_meshgrid.ravel()])
    decision_boundaries = decision_boundaries.reshape(xx_meshgrid.shape)
    out = axes_object.contourf(
        xx_meshgrid, yy_meshgrid, decision_boundaries, **params)
    return out


# Read csv with pandas
WINE_DATA = pd.read_csv('data/winequality-white.csv', sep=';')

output(WINE_DATA)

k_neighbors(WINE_DATA)

dec_tree_class(WINE_DATA)

svm_classification(WINE_DATA)

print('Finished')
